import React, { Component } from 'react';
import { AppRegistry } from "react-native";
import { Ionicons } from '@expo/vector-icons'
import { DrawerActions } from 'react-navigation-drawer';
import {createAppContainer, createDrawerNavigator, createStackNavigator} from 'react-navigation';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import MainScreen from './src/screens/MainScreen';
import DetailScreen from './src/screens/DetailScreen';
import ProductDetailScreen from './src/screens/ProductDetailScreen';

const AppDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: MainScreen,
    name: 'MainScreenStack',
    navigationOptions: ({ navigation }) => ({
      title: 'Productos',
      headerTitleStyle: { color: 'black', fontFamily:'light' },
      drawerLabel: 'Productos',
      drawerIcon: () => (
          <Ionicons name="ios-home" size={20} />
      )
    })
  },
  dePrimera: {
    screen: DetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'dePrimera',
      drawerLabel: 'dePrimera',
      drawerIcon: () => (
          <Ionicons name="ios-restaurant" size={20} />
      )
    })
  },
});


const StackNavigator = createStackNavigator({
  DrawerNavigator: {
    screen: AppDrawerNavigator,
    navigationOptions: ({ navigation }) => {

      const { state } = navigation;
      if(state.isDrawerOpen) {
        return {
          title: 'dePrimera',
          headerLeft: () => (
              <TouchableOpacity onPress={() => {navigation.dispatch(DrawerActions.toggleDrawer())}}>
                <Ionicons name="ios-close" style={styles.menuClose} size={36} />
              </TouchableOpacity>
          )
        }
      }
      else {
        return {
          title: 'dePrimera',
          headerLeft: () => (
              <TouchableOpacity onPress={() => {navigation.dispatch(DrawerActions.toggleDrawer())}}>
                <Ionicons name="ios-menu" style={styles.menuOpen} size={32} />
              </TouchableOpacity>
          )
        }
      }
    }
  },
  ProductDetailScreen: {
    screen: ProductDetailScreen,
  }

});

const App = createAppContainer(StackNavigator);

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuOpen: {
    marginLeft: 10,
    marginTop: 10
  },
  menuClose: {
    marginLeft: 14,
    marginTop: 10
  }
});
