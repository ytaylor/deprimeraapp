import {API_GET_PRODUCTS, PUBLIC_IMAGES} from "../config/const";

const processProduct = product => ( {
        id: `${product.id}`,
        name: `${product.namesupplier}`,
        phone: `${product.telef}`,
        description: `${product.description_slogan}\n ${product.discount}` ,
        image: `${PUBLIC_IMAGES}/${product.image_path}`,
        unit: `${product.unit}`,
        price: `${product.unit_price}`
    });

export const fetchProducts = async () =>{
    console.log(API_GET_PRODUCTS);

    try {
        const response = await fetch(API_GET_PRODUCTS, {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        });
        console.log(response.status);
        const results = await response.json();
        console.log(results);
        return results.map(processProduct);


    }catch(error){
        console.log(error);
        return [];
    }
};