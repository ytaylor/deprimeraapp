import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Dimensions,
    Alert,
    ScrollView
} from 'react-native';
import { Linking } from 'expo';

import {fetchProducts} from '../data/api_calls'



export default class MainScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Productos',
        };
    };

    componentDidMount() {
      // this.getProducts();
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:false,
            userSelected:[],
            data: [
                {id:"1", unit:"manojo de 2 kg", name: "Esparragos a domicilio", phone:'60956508', description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",image:"https://okdiario.com/img/2018/10/23/receta-de-salteado-de-esparragos-verdes-655x368.jpg", price:10},
                {id:"2", unit:"manojo de 2 kg", name: "Piñas Sofia",  phone:'60956508', description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", image:"https://mysuenos.com/wp-content/uploads/2018/01/pinas-maduras.jpg", price:8},
                {id:"3", unit:"manojo de 2 kg", name: "Zanahorias Top", phone:'60956508', description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", image:"https://www.conasi.eu/blog/wp-content/uploads/2018/07/zanahorias-y-tomar-el-sol.jpg", price:7} ,
                {id:"4", unit:"manojo de 2 kg", name: "Naranjas XXL",  phone:'60956508', description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",  image:"https://www.65ymas.com/uploads/s1/23/51/17/naranjas-beneficios-contraindicaciones-y-recomendaciones-con-ellas.jpeg", price:10} ,
                {id:"5", unit:"manojo de 2 kg", name: "Carniceria Robledo", phone:'60956508',  description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ", image:"https://s1.eestatic.com/2018/05/24/ciencia/Alimentacion-Nutricion-Dietista_-_nutricionista-Ciencia_309731115_79145490_1024x576.jpg", price:0} ,
            ]
        };
    }

    getProducts = async ()=> {
        try {
            const results = await fetchProducts();
            this.setState({data: results});
        }catch (error) {
            this.setState({data: []});
        }
    };


    clickEventListener = (item) => {
        this.props.navigation.navigate("ProductDetailScreen", {product: item});
    };

    clickEventListenerPhone = (item) => {
        Linking.openURL('tel:'+item.phone);
    };

    render() {
        var callIcon = "https://img.icons8.com/color/48/000000/phone.png";
        return (
            <View style={styles.container}>

                <FlatList
                    style={styles.contentList}
                    columnWrapperStyle={styles.listContainer}
                    data={this.state.data}
                    keyExtractor= {(item) => {
                        return item.id;
                    }}
                    renderItem={({item}) => {
                        return (
                            <TouchableOpacity key={item.id} style={styles.card} onPress={() => {this.clickEventListener(item)}}>
                                <Image style={styles.image} source={{uri: item.image}}/>
                                <View style={styles.cardContent}>
                                    <Text style={styles.name}>{item.name}</Text>
                                    <Text style={styles.count}>{item.unit}: {item.price}€</Text>
                                    <Text style={styles.description}>{item.description} </Text>

                                    <View style={styles.viewButtons}>
                                    <TouchableOpacity style={styles.followButton} onPress={()=> this.clickEventListener(item)}>
                                        <Text style={styles.followButtonText}>Ver más</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.followButton} onPress={()=> this.clickEventListenerPhone(item)}>
                                        <Image style={[styles.icon, { marginLeft: 5 }]} source={{uri: callIcon}}/>
                                        <Text style={styles.followButtonText}>{item.phone}</Text>
                                    </TouchableOpacity>

                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:20,
        backgroundColor:"#ebf0f7"
    },
    viewButtons:{
      flexDirection: 'row'
    },
    contentList:{
        flex:1,
    },
    cardContent: {
        marginLeft:20,
        marginTop:10,
        width: '68%',
    },
    image:{
        width:90,
        height:90,
        borderRadius:0,
        borderWidth:2,
        borderColor:"#ebf0f7"
    },

    card:{
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 0,
        elevation: 12,

        marginLeft: 10,
        marginRight: 10,
        marginTop:10,
        backgroundColor:"white",
        padding: 10,
        flexDirection:'row',
        borderRadius:0,
    },

    name:{
        fontSize:18,
        flex:1,
        alignSelf:'center',
        color:"#3399ff",
        fontWeight:'bold'
    },
    count:{
        fontSize:14,
        flex:1,
        alignSelf:'center',
        color:"#6666ff"
    },
    description:{
        fontSize:12,
        flex:1,
        alignSelf:'center',
        color:'grey'
    },

    followButton: {
        marginTop:10,
        marginLeft: 5,
        height:30,
        width:100,
        padding:5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:0,
        backgroundColor: "white",
        borderWidth:1,
        borderColor:"#dcdcdc",
    },
    followButtonText:{
        color: "grey",
        fontSize:12,
    },
    icon:{
        height: 20,
        width: 20,
    }
});