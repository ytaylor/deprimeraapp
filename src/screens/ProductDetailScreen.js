import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Dimensions,
    Alert,
    ScrollView
} from 'react-native';

import MapView from 'react-native-maps';
import Linking from 'expo';

export default class ProductDetailScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Detalle del Producto',
        };
    };


    constructor(props) {
        super(props);
        const { navigation } = this.props;
        const product = navigation.getParam('product');

        this.state = {
            modalVisible:false,
            userSelected:[],
            product: product,
        };
    }

    componentDidMount() {
    }

    clickEventListenerPhone = (item) => {
        Linking.openURL('tel:'+item.phone);
    };

    render() {
        var mainImage = this.state.product.image;
        var callIcon = "https://img.icons8.com/color/48/000000/phone.png";

        return (
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    <View style={styles.card}>
                        <View style={styles.cardHeader}>
                            <Text style={styles.name}>{this.state.product.name}</Text>
                        </View>
                        <View style={styles.cardContent}>
                            <View style={styles.header}>
                                <View>
                                    <Image style={styles.mainImage} source={{uri:mainImage}}/>
                                </View>
                            </View>
                        </View>
                        <View style={styles.cardHeader}>
                            <Text style={styles.name}>Precio/Kg: {this.state.product.price} €</Text>
                        </View>
                        <View style={styles.cardContent}>
                            <Text style={styles.description}>{this.state.product.description}</Text>
                        </View>
                        <View style={styles.cardContent}>
                            <View style={styles.header}>

                            <MapView style={styles.mapStyle} />
                            </View>
                        </View>

                        <View style={styles.cardContent}>
                            <TouchableOpacity style={styles.followButton} onPress={()=> this.clickEventListenerPhone(this.state.product)}>
                                <Image style={[styles.icon, { marginLeft: 5 }]} source={{uri: callIcon}}/>
                                <Text style={styles.name}>{this.state.product.phone}</Text>
                            </TouchableOpacity>
                        </View>

                    </View>




                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:20,
        backgroundColor:"#ebf0f7",
    },
    content:{
        marginLeft:10,
        marginRight:10,
        marginTop:20,
    },
    header:{
        flexDirection:'row',
    },
    mainImage:{
        width:250,
        height:250,
    },
    mapStyle: {
        width:250,
        height:250,
    },
    smallImagesContainer:{
        flexDirection:'column',
        marginLeft:30
    },
    smallImage:{
        width:60,
        height:60,
        marginTop:5,
    },
    btnColor: {
        height:40,
        width:40,
        borderRadius:40,
        marginHorizontal:3
    },
    contentColors:{
        flexDirection:'row',
    },
    name:{
        fontSize:22,
        color:"#696969",
        fontWeight:'bold',
    },
    price:{
        marginTop:10,
        fontSize:18,
        color:"green",
        fontWeight:'bold'
    },
    description:{
        fontSize:18,
        color:"#696969",
    },
    shareButton: {
        marginTop:10,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:30,
        backgroundColor: "#00BFFF",
    },
    shareButtonText:{
        color: "#FFFFFF",
        fontSize:20,
    },

    /******** card **************/
    card:{
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
        alignItems: 'center',
        marginVertical: 5,
        backgroundColor:"white",
        marginHorizontal: 5,
    },
    cardContent: {
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    cardHeader:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 12.5,
        paddingHorizontal: 16,
        borderBottomLeftRadius: 1,
        borderBottomRightRadius: 1,
    },
    cardTitle:{
        color:"#00BFFF"
    },
        followButton: {
            marginTop:10,
            marginLeft: 5,
            height:30,
            width:250,
            padding:5,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius:0,
            backgroundColor: "white",
        },
    icon:{
        height: 20,
        width: 20,
    }
});