import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Image,
    Alert,
    ScrollView, requireNativeComponent,
} from 'react-native';

export default class DetailScreen extends Component {

    constructor(props) {
        super(props);
    }

    onClickListener = (viewId) => {
        Alert.alert("Alert", "Button pressed ");
    }

    render() {
        return (
            <ScrollView style={styles.scrollContainer}>
                <View style={styles.container}>
                    <Image style={styles.logo} source={require('../../assets/dep.png')}/>
                    <View style={styles.descriptionContent}>
                        <Text style={styles.description}>
                            Encuentra todo tipo de materias primas de proximidad y calidad, los productores  estarán encantados de venderte directamente y además ¡te lo llevan a casa!
                            Las medidas higiénico sanitarias están aseguradas con la mayor eficacia ...¡más cómodo  y seguro imposible!
                        </Text>

                        <Text style={[styles.description,{fontWeight: 'bold'}]}> Escríbele por WhatsApp a su teléfono móvil, le mandas la ubicación de tu casa y concretas con él/ ella.</Text>

                        <Text style={styles.description}>

                            Estimado productor, miles de clientes de tu comunidad quieren conocerte, amplia tu negocio, ¡hazlo más visible y accede a la venta directa!
                            Escríbenos a  <Text style={{fontWeight: 'bold'}}>info@deprimera.org</Text> o <Text style={{fontWeight: 'bold'}}>983048173</Text> estaremos encantados de agregarte a nuestra familia

                        </Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    scrollContainer:{
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    logo:{
        width:'90%',
        height:120,
        resizeMode: 'stretch',
        /*justifyContent: 'center',*/
        marginBottom:0,
        marginTop:30,
    },
    companyName: {
        fontSize:32,
        fontWeight: '600',
        color: 'grey',
    },
    slogan:{
        fontSize:18,
        fontWeight: '600',
        color: '#228B22',
        marginTop:10,
    },
    descriptionContent:{
        padding:10
    },
    description:{
        fontSize:16,
        textAlign:'center',
        marginTop:10,
        color: 'grey',
    },
    buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:100,
        borderRadius:30,
    },
    sendButton: {
        backgroundColor: "#FFFFFF",
    },
    buttonText: {
        color: '#EE82EE',
    }
});